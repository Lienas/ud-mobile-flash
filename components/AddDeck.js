import React from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Alert, ActivityIndicator} from "react-native";
import {commonStyles, guid} from "../utils/helpers";
import {connect} from "react-redux";
import {addDeck} from "../actions";

class AddDeck extends React.Component {

    state = {
        value: '',
        animating: false
    }

    onChangeText = (text) => {
        this.setState({
            value: text
        })

    }

    setAnimating = (animate) => {
        this.setState({
            animating: animate
        })
    }

    submitHandler = () => {
        const {value} = this.state
        const {dispatch, navigation} = this.props

        if (value) {
            console.log("submitted Value", value)
            this.setAnimating(true)
            const deck = {
                [guid()]: {
                    title: value,
                    questions: []
                }
            }
            dispatch(addDeck(deck))
            navigation.navigate('decks')

        } else {
            Alert.alert("Message", "please enter a name")
        }
    }

    render() {

        const {animating, value} = this.state

        if (animating) {

            return (
                <View style={commonStyles.spinnerScreen}>
                    <ActivityIndicator animating={animating} color='#fff' size='large'/>
                </View>)

        } else {

            return (
                <View style={commonStyles.containerSpace}>
                    <View style={{justifyContent: 'flex-start'}}>
                        <Text style={commonStyles.H1}>Add a new Deck</Text>
                        <View style={{flexDirection: 'row'}}>
                            <TextInput
                                value={value}
                                placeholder='Name'
                                placeholderTextColor='gray'
                                style={styles.textInput}
                                onChangeText={(text) => this.onChangeText(text)}/>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginLeft: 25, marginRight: 25}}>
                        <TouchableOpacity
                            style={value ? commonStyles.buttonSuccess : commonStyles.buttonDisabled}
                            onPress={() => this.submitHandler()}>
                            <Text style={commonStyles.buttonTxt}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    textInput: {
        flex: 1,
        backgroundColor: 'lightgray',
        fontSize: 25,
        height: 45,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginLeft: 25,
        marginRight: 25,
        padding: 10
    }
})


export default connect()(AddDeck);
