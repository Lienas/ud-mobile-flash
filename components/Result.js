import React from 'react';
import {View, Text, Button} from 'react-native'
import {commonStyles} from "../utils/helpers";
import {MaterialCommunityIcons} from '@expo/vector-icons'


function Result(props) {

    const {navigation, route} = props
    const {name, id, correct, incorrect} = route.params
    let icon = 'emoticon-neutral-outline'
    let iconColor = 'white'

    if (incorrect === 0) {
        icon = 'emoticon-happy-outline'
        iconColor = 'green'
    } else if (correct === 0) {
        icon = 'emoticon-sad-outline'
        iconColor = 'red'
    }

    return (
        <View style={[commonStyles.containerSpace, {alignItems: 'stretch'}]}>
            <View style={{alignItems:'center'}}>
                <Text style={commonStyles.H1}>Result</Text>
                <View style={{marginBottom: 35}}>
                    <MaterialCommunityIcons name={icon} size={80} color={iconColor}/>
                </View>
                <Text style={commonStyles.H2}>
                    You had {correct} correct and {incorrect} incorrect answers !
                </Text>
            </View>
            <View style={
                {
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginBottom: 25,
                }
            }>
                <Button
                    title='Restart Quiz'
                    onPress={() =>
                        navigation.push('quiz',
                            {name: name, id: id})}/>
                <Button title='Back to Deck'
                        onPress={() =>
                            navigation.navigate('deckDetail',
                                {name: name, id: id})}/>

            </View>
        </View>);
}

export default Result;
