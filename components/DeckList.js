import React, {Component} from 'react';
import {Text, ScrollView, View, TouchableOpacity} from "react-native";
import {commonStyles} from "../utils/helpers";
import Deck from "./Deck";
import {connect} from "react-redux";

class DeckList extends Component {

    addCardHandler = () => {
        const {navigation} = this.props
        navigation.navigate('addDeck')
    }

    render() {

        const {decks} = this.props

        return (
            <View style={commonStyles.container}>
                <View>
                    <Text style={commonStyles.H1}>
                        Mobile Flashcards
                    </Text>
                    <ScrollView style={{marginBottom: 50}} indicatorStyle='white'>
                        {Object.keys(decks).map((id) => {
                            return <Deck key={id}
                                         id={id}
                                         navigation={this.props.navigation}/>
                        })}
                    </ScrollView>
                    <TouchableOpacity style={commonStyles.buttonSuccess} onPress={this.addCardHandler}>
                        <Text style={commonStyles.buttonTxt}>Add Deck</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

function mapStateToProps(decks) {
    return {
        decks
    }
}

export default connect(mapStateToProps)(DeckList)
