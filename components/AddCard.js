import React from 'react';
import {View, Text, TouchableOpacity, TextInput, Alert, StyleSheet, ActivityIndicator} from 'react-native'
import {commonStyles} from "../utils/helpers";
import {connect} from "react-redux";
import {addCard} from "../actions";

class AddCard extends React.Component {

    state = {
        question: '',
        answer: '',
        animating: ''
    }

    setQuestion = (text) => {
        this.setState({
            question: text
        })
    }

    setAnswer = (text) => {
        this.setState({
            answer: text
        })
    }

    setAnimating = (animate) => {
        this.setState(
            {
                animating: animate
            }
        )
    }

    submitHandler = () => {

        const {question, answer} = this.state
        const {dispatch, navigation, id} = this.props

        if (question && answer) {
            this.setAnimating(true)
            const card = {
                question: question,
                answer: answer
            }
            dispatch(addCard(card, id))
            navigation.goBack()

        } else {
            Alert.alert("Warning", "please fill out the complete form")
        }
    }

    render() {
        const {animating, question, answer} = this.state

        if (animating) {
            return (
                <View style={commonStyles.spinnerScreen}>
                    <ActivityIndicator animating={animating} color='#fff' size='large'/>
                </View>)
        } else {
            return (
                <View style={commonStyles.containerSpace}>
                    <View style={{justifyContent: 'flex-start'}}>
                        <Text style={commonStyles.H1}>Add a new Card</Text>
                        <View style={{flexDirection: 'column'}}>
                            <TextInput
                                value={question}
                                placeholder='Question'
                                placeholderTextColor='gray'
                                style={styles.textInput}
                                onChangeText={(text) => this.setQuestion(text)}/>
                            <TextInput
                                value={answer}
                                placeholder='Answer'
                                placeholderTextColor='gray'
                                style={styles.textInput}
                                onChangeText={(text) => this.setAnswer(text)}/>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginLeft: 25, marginRight: 25}}>
                        <TouchableOpacity
                            style={question && answer ? commonStyles.buttonSuccess : commonStyles.buttonDisabled}
                            onPress={() => this.submitHandler()}>
                            <Text style={commonStyles.buttonTxt}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    textInput: {
        backgroundColor: 'lightgray',
        fontSize: 15,
        height: 45,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 10,
        padding: 10
    }
})

function mapStateToProps(decks, {route}) {
    const {id} = route.params
    return {
        id,
        deck: decks[id]
    }
}

export default connect(mapStateToProps)(AddCard);
