import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from "react-native";
import {commonStyles} from "../utils/helpers";
import {connect} from "react-redux";

class Deck extends React.Component {

    showDetails = () => {
        const {navigation, deck, id} = this.props
        navigation.navigate('deckDetail', {name: deck.title, id: id})
    }

    render() {
        const {deck, id} = this.props
        const cardsCount = deck.questions.length

        return (
            <TouchableOpacity style={styles.row} onPress={() => this.showDetails()}>
                <View style={{flexDirection: 'column'}}>
                    <Text style={commonStyles.H2}>{deck.title}</Text>
                    <Text style={commonStyles.H3}>
                        {cardsCount} {cardsCount === 1 ? ' card' : ' cards'}
                    </Text>
                </View>
                <Text style={commonStyles.H3}>Detail</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingRight: 15
    }
})

function mapStateToProps(decks, {id}) {
    return {
        deck: decks[id]
    }
}

export default connect(mapStateToProps)(Deck);
