import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Button, Animated, Alert} from 'react-native'
import {commonStyles} from "../utils/helpers";
import {connect} from "react-redux";
import {deleteDeck} from "../actions";

class DeckDetail extends Component {

    state = {
        fadeAnim: new Animated.Value(0)
    }

    componentDidMount() {
        this.fadeIn()
    }

    fadeIn = () => {
        Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 2000,
            useNativeDriver: true
        }).start()
    }

    addCardHandler = () => {
        const {navigation, route} = this.props
        const {name, id} = route.params
        navigation.navigate('addCard', {name: name, id: id})
    }

    startQuizHandler = () => {
        const {navigation, route} = this.props
        const {name, id} = route.params
        navigation.navigate('quiz', {name: name, id: id})
    }

    deleteDeckHandler = () => {
        const {navigation, route, dispatch, decks} = this.props
        const {id} = route.params

        Alert.alert(
            "Warning",
            "You are about deleting desk: " + decks[id].title,
            [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: "Confirm",
                    onPress: () => {
                        dispatch(deleteDeck(id))
                        navigation.navigate('decks')
                    }
                }
            ])
    }

    render() {
        const {route, decks} = this.props
        const {id, name} = route.params
        const deck = decks[id]
        const cardCount = deck ? deck.questions.length : 0

        return (
            <Animated.View style={[commonStyles.containerSpace, {opacity: this.state.fadeAnim}]}>
                <View style={{alignItems: 'center'}}>
                    <Text style={commonStyles.H1}>{name}</Text>
                    <Text style={commonStyles.H3}>
                        {cardCount} {cardCount === 1 ? ' Card' : ' Cards'}
                    </Text>
                </View>
                <View style={{flexDirection: 'row', marginLeft: 25, marginRight: 25}}>
                    <View style={{flex: 1}}>
                        <TouchableOpacity style={commonStyles.buttonSuccess} onPress={this.addCardHandler}>
                            <Text style={commonStyles.buttonTxt}>Add Card</Text>
                        </TouchableOpacity>

                        {cardCount > 0
                            ? <TouchableOpacity style={commonStyles.buttonPrimary} onPress={this.startQuizHandler}>
                                <Text style={commonStyles.buttonTxt}>Start Quiz</Text>
                            </TouchableOpacity>
                            : <View style={commonStyles.buttonDisabled}>
                                <Text style={{color: 'blue', fontSize: 20}}>Add Cards to Start the Quiz</Text>
                            </View>
                        }

                        <View style={{marginBottom: 20}}>
                            <Button
                                title='Delete Deck'
                                color='red'
                                onPress={this.deleteDeckHandler}/>
                        </View>
                    </View>
                </View>
            </Animated.View>
        );
    }
}

function mapStateToProps(decks) {
    return {
        decks
    }

}

export default connect(mapStateToProps)(DeckDetail);
