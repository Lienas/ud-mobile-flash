import React from 'react';
import {View, Text, TouchableOpacity, Button} from 'react-native'
import {clearLocalNotifications, commonStyles, setLocalNotification} from "../utils/helpers";
import {connect} from "react-redux";

class Quiz extends React.Component {

    state = {
        questionIndex: 0,
        showAnswer: false,
        finished: false,
        correctAnswers: 0,
        incorrectAnswers: 0
    }

    addAnswer = (isCorrect) => {

        const {questionIndex: idx} = this.state
        const {questions} = this.props
        const questCount = questions.length
        const lastQuestion = idx === (questCount - 1)

        if (isCorrect) {
            this.setState((prev) => {
                return {
                    finished: lastQuestion,
                    showAnswer: false,
                    correctAnswers: prev.correctAnswers + 1
                }
            })
        } else {
            this.setState((prev) => {
                return {
                    finished: lastQuestion,
                    showAnswer: false,
                    incorrectAnswers: prev.incorrectAnswers + 1
                }
            })
        }

        if (!lastQuestion) {
            this.setState((prev) => {
                return {
                    questionIndex: prev.questionIndex + 1
                }
            })
        }
    }

    showAnswer = () => {
        this.setState((prev) => {
            return {
                showAnswer: !prev.showAnswer
            }
        })
    }


    render() {

        const {navigation, questions, deck, id} = this.props
        const {questionIndex: idx, showAnswer, finished, correctAnswers, incorrectAnswers} = this.state

        const question = questions[idx]
        const questCount = questions.length

        navigation.setOptions({
            title: deck.title + ' ' + (idx + 1) + '/' + questCount
        })

        if (finished) {

            //Clear Notification
            clearLocalNotifications()
            //Set Notification for the next day
            setLocalNotification()

            //Show Result
            navigation.navigate('result',
                {
                    name: deck.title,
                    id: id,
                    correct: correctAnswers,
                    incorrect: incorrectAnswers
                })
        }

        return (
            <View style={commonStyles.containerSpace}>
                <View style={{alignItems: 'center'}}>
                    <Text style={commonStyles.H1}>Quiz</Text>
                    <Text style={[commonStyles.H2, {marginBottom: 25}]}>
                        {showAnswer ? question.answer : question.question}
                    </Text>
                    <Button
                        title={showAnswer ? 'Show Question' : 'Show Answer'}
                        onPress={this.showAnswer}/>
                </View>
                <View style={{flexDirection: 'row', marginLeft: 25, marginRight: 25}}>
                    <View style={{flex: 1}}>
                        <TouchableOpacity style={commonStyles.buttonSuccess} onPress={() => this.addAnswer(true)}>
                            <Text style={commonStyles.buttonTxt}>Correct</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={commonStyles.buttonPrimary} onPress={() => this.addAnswer(false)}>
                            <Text style={commonStyles.buttonTxt}>Incorrect</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        );
    }

}


function mapStateToProps(decks, {route}) {
    const {id} = route.params
    return {
        id,
        deck: decks[id],
        questions: decks[id].questions
    }

}

export default connect(mapStateToProps)(Quiz);
