import {StyleSheet} from 'react-native'
import * as Permissions from 'expo-permissions'
import {Notifications}  from 'expo'
import AsyncStorage from "@react-native-community/async-storage";

const NOTIFICATION_KEY = '@flash:notifications'

/**
 * Generates a GUID string for the cards
 *
 * @returns {string} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser.
 * @link http://slavik.meltser.info/?p=142
 */
export function guid() {
    function _p8(s) {
        const p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }

    return _p8() + _p8(true) + _p8(true) + _p8();
}

export const commonStyles = StyleSheet.create({
    H1: {
        color: '#fff',
        fontSize: 40,
        marginTop: 75,
        marginBottom: 50,
        padding: 5
    },
    H2: {
        color: '#fff',
        fontSize: 25,
        justifyContent: 'flex-start'
    },
    H3: {
        color: '#dcdcdc',
        fontSize: 15,
        fontStyle: 'italic'
    },
    headerTitle: {
        fontSize: 15
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#000000',
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    containerSpace: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#000000',
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    spinnerScreen: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#000'
    },
    buttonSuccess: {
        alignItems: "center",
        borderRadius: 7,
        backgroundColor: "#32D74B",
        padding: 15,
        marginBottom: 20,
        //flexGrow: 1
    },
    buttonDisabled: {
        alignItems: "center",
        borderRadius: 7,
        backgroundColor: "gray",
        padding: 15,
        marginBottom: 20,
        flexGrow: 1
    },
    buttonPrimary: {
        alignItems: "center",
        borderRadius: 7,
        backgroundColor: "#0A84FF",
        padding: 15,
        marginBottom: 20,
        flexGrow: 1
    },

    buttonTxt: {
        color: '#fff',
        fontSize: 25
    }
})

/**
 * for development purposes
 *
 * @param time
 * @returns {Promise<unknown>}
 */
export const simulateSave = (time) => {
    console.log("save new deck")
    return new Promise((resolve) =>
        setTimeout(resolve, time))
}

export function clearLocalNotifications() {
    AsyncStorage.removeItem(NOTIFICATION_KEY)
        .then(Notifications.cancelAllScheduledNotificationsAsync)

}

export function setLocalNotification() {
    AsyncStorage.getItem(NOTIFICATION_KEY)
        .then(JSON.parse)
        .then((data) => {
            if (data === null) {
                Permissions.askAsync(Permissions.NOTIFICATIONS)
                    .then(({status}) => {
                        if (status === 'granted') {

                            //todo: change to 'expo-notifications'
                            //todo: fix notifications on ios, when app is in foreground
                            Notifications.cancelAllScheduledNotificationsAsync()

                            //remind daily at 10
                            //daily reminders are deprecated !!!
                            // todo: fix with next sdk of expo > 38 !!!!
                            let tomorrow = new Date()
                            tomorrow.setDate(tomorrow.getDate() + 1)
                            tomorrow.setHours(10)
                            tomorrow.setMinutes(0)

                            Notifications.scheduleLocalNotificationAsync(
                                 createLocalNotification(),
                                {
                                    time: tomorrow,
                                    repeat: 'day'
                                }
                            )
                            AsyncStorage.setItem(NOTIFICATION_KEY, JSON.stringify(true))
                            console.log("Notification set to ", tomorrow)
                        }
                    })
            }
        })
}

export function createLocalNotification() {
    return {
        title: 'Reminder',
        body: "👋 don't forget to start a quiz",
        ios: {
            sound: true
        },
        android: {
            sound: true,
            priority: 'high',
            sticky: false,
            vibrate: true
        }

    }
}


