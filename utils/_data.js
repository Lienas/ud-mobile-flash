/**
 * data for decks
 * @type {{JavaScript: {questions: [{question: string, answer: string}], title: string}, React: {questions: [{question: string, answer: string}, {question: string, answer: string}], title: string}}}
 * @private
 */

export const _decks = {
    React: {
        title: 'React',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    JavaScript: {
        title: 'JavaScript',
        questions: [
            {
                question: 'What is a closure?',
                answer: 'The combination of a function and the lexical environment within which that function was declared.'
            }
        ]
    },
    Test: {
        title: 'Test',
        questions: []
    },
    Test2: {
        title: 'Test2',
        questions: []
    },
    Test3: {
        title: 'Test3',
        questions: []
    },
    Test4: {
        title: 'Test4',
        questions: []
    },
    Test5: {
        title: 'Test5',
        questions: []
    },
    Test6: {
        title: 'Test6',
        questions: []
    },

}
