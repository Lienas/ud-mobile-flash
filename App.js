import React,  {useEffect} from 'react';
import DeckList from "./components/DeckList";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from '@react-navigation/stack'
import AddDeck from "./components/AddDeck";
import DeckDetail from "./components/DeckDetail";
import AddCard from "./components/AddCard";
import Quiz from "./components/Quiz";
import Result from "./components/Result";
import {Provider} from 'react-redux'
import {createStore} from "redux";
import reducer from './reducers'
import AsyncStorage from "@react-native-community/async-storage";
import {receiveDecks} from "./actions";
import {setLocalNotification} from "./utils/helpers";
import {StatusBar,Platform} from "react-native";

const Stack = createStackNavigator()
const store = createStore(reducer)
const STORAGE_KEY = '@flash-storage'


store.subscribe(() => AsyncStorage.setItem(
    STORAGE_KEY, JSON.stringify(store.getState())
    )
)

// checking the async storage for previous stored data
AsyncStorage.getItem(STORAGE_KEY).then(JSON.parse).then((data) => {
    store.dispatch(receiveDecks(data))
})

export default function App() {

    useEffect(() => {
        setLocalNotification()
    })

    return (
        <Provider store={store}>
            <NavigationContainer>
                <StatusBar barStyle= {Platform.OS === 'ios' ? 'light-content' :'dark-content' }/>
                <Stack.Navigator initialRouteName='decks'>
                    <Stack.Screen
                        name='decks'
                        component={DeckList}
                        options={
                            {
                                title: 'List Decks',
                                headerTitle: 'Home',
                                headerStyle: {
                                    backgroundColor: '#1c1c1c',
                                },
                                headerTintColor: '#fff',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },
                            }
                        }/>
                    <Stack.Screen
                        name='addDeck'
                        component={AddDeck}
                        options={
                            {
                                title: 'Add Deck',
                                headerStyle: {
                                    backgroundColor: '#32D74B',
                                },
                                headerTintColor: '#fff',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },
                            }

                        }/>
                    <Stack.Screen
                        name='deckDetail'
                        component={DeckDetail}
                        options={({route}) => ({
                            title: route.params.name,
                            headerStyle: {
                                backgroundColor: '#1c1c1c',
                            },
                            headerTintColor: '#fff',
                            headerTitleStyle: {
                                fontWeight: 'bold',
                            },
                        })}/>
                    <Stack.Screen
                        name='addCard'
                        component={AddCard}
                        options={({route}) =>
                            ({
                                title: route.params.name,
                                headerStyle: {
                                    backgroundColor: '#32D74B',
                                },
                                headerTintColor: '#fff',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },
                            })}/>
                    <Stack.Screen
                        name='quiz'
                        component={Quiz}
                        options={({route}) => (
                            {
                                title: route.params.name,
                                headerBackTitle: 'back',
                                headerStyle: {
                                    backgroundColor: '#32D74B',
                                },
                                headerTintColor: '#fff',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },
                            })}/>
                    <Stack.Screen
                        name='result'
                        component={Result}
                        options={({route}) => (
                            {
                                title: route.params.name,
                                headerBackTitle: 'back',
                                headerStyle: {
                                    backgroundColor: '#32D74B',
                                },
                                headerTintColor: '#fff',
                                headerTitleStyle: {
                                    fontWeight: 'bold',
                                },
                            })}/>

                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
}

