# Mobile Flashcards

<p>
  <!-- iOS -->
  <a href="https://itunes.apple.com/app/apple-store/id982107779">
    <img alt="Supports Expo iOS" longdesc="Supports Expo iOS" src="https://img.shields.io/badge/iOS-4630EB.svg?style=flat-square&logo=APPLE&labelColor=999999&logoColor=fff" />
  </a>
  <!-- Android -->
  <a href="https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=blankexample">
    <img alt="Supports Expo Android" longdesc="Supports Expo Android" src="https://img.shields.io/badge/Android-4630EB.svg?style=flat-square&logo=ANDROID&labelColor=A4C639&logoColor=fff" />
  </a>  
</p>

## 🚀 How to use

- Install packages with `yarn` or `npm install`.
- Run `expo start` or `yarn start` to start the app.
- Open the project in Expo-Client:
  - iOS: [Client iOS](https://itunes.apple.com/app/apple-store/id982107779)
  - Android: [Client Android](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=blankexample)
  
## 💙 Features
- create Decks to hold cards
- add cards to decks
- carry out a quiz
- toggle between question and answers
- mark an answer as correct or incorrect
- show results and rerun quiz
- daily notification at 1o a.m. to run a quiz
- persist the data


## 📝 Notes

- app is optimized for ios, but works fine on android as well
- data is persisted, when state in redux-store changes
- notifications
  - work, but daily notifications are deprecated for ios (if a warning appears- just ignore)
  - notifications should be used from **expo-notifications** instead of **expo** (takes some time to learn the new API 😥- next release)
  - on ios notification are not visible, while app is in the foreground (fix in next release)


