import {ADD_DECK, ADD_CARD, RECEIVE_DECKS, DELETE_DECK} from '../actions'

function decks(state = {}, action) {
    switch (action.type) {
        case RECEIVE_DECKS:
            return {
                ...state,
                ...action.decks
            }
        case ADD_DECK:
            return {
                ...state,
                ...action.deck
            }
        case ADD_CARD:
            return {
                ...state,
                [action.key]: {
                    ...state[action.key],
                    questions: state[action.key].questions.concat(action.card)
                }
            }
        case DELETE_DECK:
            const newState = Object.keys(state)
                .filter((key) => key !== action.key)
                .reduce((res, cur) => {
                    res[cur] = state[cur]
                    return res}, {}
                )
            return {
                ...newState
            }
        default:
            return state
    }
}

export default decks
